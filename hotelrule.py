"""Hotel Management"""

class HotelRule():
    def __init__(self):
        self.data = {}
        self.add_hotel()

    def add_hotel(self):
        """method to add Hotel"""
        HotelName = input("Enter The Hotel Name: ")
        if self.data.get(HotelName, False):
            print("Hotel with same name already exist.")
            self.add_hotel_again()
        else:
            self.data[HotelName] = {}
            self.add_room(HotelName)

    def add_room(self, HotelName: str):
        """method to add Room"""
        RoomNo = input("Enter Room Number: ")
        self.data[HotelName][RoomNo] = {"Items": [], "ItemsList": [], "Price": 0}
        i = 0
        while i < 5:
            self.add_all_items(HotelName, RoomNo)
            i += 1
        self.add_items_again(HotelName, RoomNo)

    def add_all_items(self, HotelName: str, RoomNo: str):
        """method to add all Items"""
        item = input("Enter the Items of Hotel: ")
        price = input("Enter the Items prize in $ (should be a Number): ")
        self.data[HotelName][RoomNo]["Items"].append({item:price})
        self.data[HotelName][RoomNo]["ItemsList"].append(item)
        self.data[HotelName][RoomNo]["Price"] += int(price)


    def add_items(self, HotelName: str, RoomNo: str):
        """method to add Items"""
        item = input("Enter the Items of Hotel: ")
        price = input("Enter the Items prize in $ (should be a number): ")
        self.data[HotelName][RoomNo]["Items"].append({item:price})
        self.data[HotelName][RoomNo]["ItemsList"].append(item)
        self.data[HotelName][RoomNo]["Price"] += int(price)
        self.add_items_again(HotelName, RoomNo)


    def add_hotel_again(self):
        """method to re-add Hotel"""
        add_hotel = input("Do you want to add New Hotel (Yes/No): ")
        if add_hotel.lower() == "yes":
            self.add_hotel()
        elif add_hotel.lower() == "no":
            self.display()
        else:
            print("Invalid Input")
            self.add_hotel_again()

    def add_room_again(self, HotelName: str):
        """method to re-add Room"""
        add_room = input("Do you want to add more Room(Yes/No): ")
        if add_room.lower() == "yes":
            self.add_room(HotelName)
        elif add_room.lower() == "no":
            self.add_hotel_again()
        else:
            print("Invalid Input")
            self.add_room_again(HotelName)


    def add_items_again(self, HotelName: str, roomNo: str):
        """method to re-add Items"""
        add_items = input("Do you want to add more Items(Yes/No): ")
        if add_items.lower() == "yes":
            self.add_items(HotelName, roomNo)
        elif add_items.lower() == "no":
            self.add_room_again(HotelName)
        else:
            print("Invalid Input")
            self.add_items_again(HotelName, roomNo)

    def display(self, budget=0):
        """method to Display Deatils of hotels"""
        for hotel_name, room_info in self.data.items():
            context = "\nRoom No {0} have the Items like {1}, and Total Price is ${2}."
            if budget == 0:
                data = '\n\n\n{0} Hotel have following rooms\n'.format(hotel_name)
                for room, item_info in room_info.items():
                    items = ', '.join(["{0}: ${1}".format(item, price) for items in item_info["Items"] for item , price in items.items()])
                    cost = item_info['Price']
                    data += context.format(room, items, cost)
                print(data)
            else:
                data = '\n\n\n{0} Hotel have following rooms matching your budget\n'.format(hotel_name)
                availability = False
                for room, item_info in room_info.items():
                    cost = item_info['Price']
                    if cost <= budget:
                        items = ', '.join(["{0}: ${1}".format(item, price) for dic in item_info["Items"] for item , price in dic.items()])
                        availability = True
                        data += context.format(room, items, cost)
                if availability:
                    print(data)
                else:
                    print("\n\n\nNo room available for your budget in hotel {0}".format(hotel_name))
            print("\n****************$$$**************&&********************$$$*********\n")
        if budget == 0:
            self.get_user_budget()

    def get_user_budget(self):
        """method to search and display the budget of hotels"""
        print("\n***********##*************$$$***********************************\n")
        budget = int(input("Please enter your budget: "))
        if budget > 1:
            self.display(budget)
        else:
            print("Invalid Budget Range")
            self.get_user_budget()


if __name__ == '__main__': 
    HotelRule()
